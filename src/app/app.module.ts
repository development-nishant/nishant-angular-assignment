import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { CountryListComponent } from './screens/country-list/country-list.component';
import { NgMaterialModule } from './ng-material-module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FilterPipe } from './pipes/filter-pipe/filter.pipe';
import { AppHttpInterceptor } from './interceptor/app-http-interceptor';
import { NgxPaginationModule } from 'ngx-pagination';
import { NoDataFoundComponent } from './screens/no-data-found/no-data-found.component';

@NgModule({
  declarations: [
    AppComponent,
    CountryListComponent, // country list component
    NoDataFoundComponent, // component to display no data found page
    FilterPipe,  // custom filter pipe to filter country list...
  ],
  imports: [
    BrowserModule,
    NgMaterialModule, // to import all angular material ui components
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    NgxPaginationModule //pagination
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppHttpInterceptor, // Interceptor for http request call
      multi: true
    }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule { }
