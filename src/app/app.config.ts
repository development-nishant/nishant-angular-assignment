/**
 *  File to declare constants used in application
 */

export const APP_CONFIG: any = {

  APP_TITLE: "Country Search",
  REMOTE_URL: {
    FETCH_ALL_COUNTRIES: "https://restcountries.eu/rest/v2/all"
  },
  NO_DATA: {
    MSG: "No data found"
  },
  ERROR_MESSAGE: {
    MSG: "Sorry! Something went wrong."
  }
};
