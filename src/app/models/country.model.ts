
export interface Country {

  name: string;
  capital: string;
  subregion: string;
  region: string;
  flag: string;
  population: number;
  area: number;
  currencies: any[];
  languages: any[];

}
