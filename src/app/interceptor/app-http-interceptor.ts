import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {

  /**
   * Intercepts app http interceptor
   * @param request : Ajax request body
   * @param next : Http request handler to trigger ajax request
   * @returns intercept
   */
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // Handle http response success and error response to perform action accordingly...
    return next.handle(request)
      .pipe(
        tap(response => {
          if (response && response instanceof HttpResponse) {
            if (response.status === 200) {
              // any  post success operations can go here...
            }
          }
        }),
        catchError((error: HttpErrorResponse) => {
          // Handle exceptions to show/log message...
          let errorMsg = '';
          if (error.error instanceof ErrorEvent) {
            errorMsg = `Error: ${error.error.message}`;
          }
          else {
            errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
          }
          return throwError(errorMsg);
        })
      );
  }
}


