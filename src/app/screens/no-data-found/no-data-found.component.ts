import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';


@Component({
  selector: 'app-no-data-found',
  templateUrl: './no-data-found.component.html',
  styleUrls: ['./no-data-found.component.css']
})
export class NoDataFoundComponent implements OnInit {

  @Input()
  noDataMesageText: string;

  @Input()
  noDataImagePath: string;

  @Input()
  showRefreshOnNoData: boolean = false;

  @Output()
  eventEmitterOnReload: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onRefreshButtonClick() {

    this.eventEmitterOnReload.emit();
  }
}
