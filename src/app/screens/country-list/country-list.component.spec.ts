import { NgxPaginationModule } from 'ngx-pagination';
import { CountryListService } from './country-list.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CountryListComponent } from './country-list.component';
import { FilterPipe } from '../../pipes/filter-pipe/filter.pipe';
import { AppHttpInterceptor } from '../../interceptor/app-http-interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
describe('CountryListComponent', () => {
  let component: CountryListComponent;
  let fixture: ComponentFixture<CountryListComponent>;
  let countryListService: CountryListService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, NgxPaginationModule],
      declarations: [CountryListComponent, FilterPipe],
      providers: [CountryListService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AppHttpInterceptor, // Interceptor for http request call
          multi: true
        }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryListComponent);
    countryListService = TestBed.inject(CountryListService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeDefined();
  });
});
