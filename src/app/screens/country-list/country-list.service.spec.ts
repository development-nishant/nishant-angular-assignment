import { TestBed } from '@angular/core/testing';

import { CountryListService } from './country-list.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
describe('CountryListService', () => {
  let service: CountryListService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(CountryListService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get country list via API', () => {
    service.fetchAllCountryList().subscribe((response: any) => {
      if (response && response.length > 0) {
        expect(response.length).toBeGreaterThan(0);
      }
    });
  });
});
