import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { APP_CONFIG } from '../../app.config';

@Injectable({
  providedIn: 'root'
})
export class CountryListService {

  constructor(private httpClient: HttpClient) {

  }

  /**
   * Fetchs all country list
   * @returns all country list
   */
  fetchAllCountryList(): Observable<any> {
    return this.httpClient.get(APP_CONFIG.REMOTE_URL.FETCH_ALL_COUNTRIES);
  }
}
