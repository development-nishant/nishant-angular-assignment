import { Country } from '../../models/country.model';
import { Component, OnInit, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { CountryListService } from './country-list.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { APP_CONFIG } from '../../app.config';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'app-country-list',
  templateUrl: './country-list.component.html',
  styleUrls: ['./country-list.component.css']
})
export class CountryListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator)
  matPaginator: MatPaginator;

  searchCountryNameText: string;
  countryList: Country[];
  countryListPagination: any = {
    itemsPerPage: 10,
    currentPage: 0
  };
  noDataMesageText: string = APP_CONFIG.NO_DATA.MSG;
  showNoDataMessage: boolean = false;
  showLoader: boolean = false;
  showRefreshOnNoData: boolean = false;
  constructor(private countryListService: CountryListService) {

  }
  /**
   * after view init
   */
  ngAfterViewInit(): void {

  }


  /**
   * on init
   */
  ngOnInit(): void {

    this.fetchAllCountryList();
  }

  /**
   * Fetchs all country list
   */
  fetchAllCountryList() {
    this.showLoader = true;
    this.countryListService.fetchAllCountryList().pipe(
      finalize(() => {
        //finallize block
        this.showLoader = false;
      }),
    ).subscribe((response: any) => {
      if (response && response.length > 0) {
        this.countryList = response;
        this.showNoDataMessage = false;
      } else {
        this.showNoDataMessage = true;
        this.showRefreshOnNoData = true;

      }
    }, (err: any) => {
      //Exception of error...
      this.showNoDataMessage = true;
      this.showRefreshOnNoData = true;
      this.noDataMesageText = APP_CONFIG.ERROR_MESSAGE.MSG;
    });
  }

  /**
   * Determines whether page change on
   * @param [event] : pagination object to get currentpage, previous page info
   */
  onPageChange(event?: PageEvent) {
    if (event) {
      // Clear search text while paginating so that it can display page with fitering..
      this.searchCountryNameText = "";
      // Material pagination index starts with index-1...
      this.countryListPagination.currentPage = event.pageIndex + 1;
    }
  }
  /**
   * Determines whether search text change on
   * @param value : search string
   */
  onSearchTextChange(value) {
    // Use case : It is required to reset to first page so that filter pipe can search after pagination pipe
    this.matPaginator.firstPage();
  }
  /**
   * Determines whether reload on
   * @param event : emmiter from no data component
   */
  onReload(event) {
    this.fetchAllCountryList();
  }
  /**
 * on destroy
 */
  ngOnDestroy(): void {

  }
}
