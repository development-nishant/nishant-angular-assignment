import { Pipe, PipeTransform } from '@angular/core';
import { Country } from '../../models/country.model';

@Pipe({
  name: 'listFilterPipe',
  pure: true
})
export class FilterPipe implements PipeTransform {

    /**
     * Transforms filter pipey
     * @param args : 1st element: array list to be filtered,
     *               2nd element: search string
     *               3rd element: attribute name on which you want to filter
     * @returns final list of object
     */
    transform(...args: any[]): any[] {
    const listToBeFiltered = args[0];
    const searchText = args[1];
    const filterAttr = args[2];

    if (!listToBeFiltered) {
      return [];
    }
    if (!searchText || !filterAttr) {
      return listToBeFiltered;
    }
    // Trim search text to handle extra white space scenario...
    return listToBeFiltered.filter((el: any) => new RegExp(searchText.trim(), 'i').test(el[filterAttr]));


  }

}
