import { APP_CONFIG } from './app.config';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  appConfig: any;

  constructor() {
    this.appConfig = APP_CONFIG;
  }
}
